///Final Project
///Group 6
import 'package:flutter/material.dart';

void main() {runApp(MyApp());}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Final Project',
      home: HomnePage(),
    );
  }
}

class HomnePage extends StatefulWidget {
  @override
  _HomnePageState createState() => _HomnePageState();
}

class _HomnePageState extends State<HomnePage> {
  PageController _pageController = PageController();
  List<Widget> _screens = [ page1(), page2() ];
  int _selectedIndex = 0;

  void _onPageChanged(int index) { setState(() {_selectedIndex = index;});}
  void _onitemTapped(int selectedIndex) {_pageController.jumpToPage(selectedIndex);}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blueGrey[500],
        onTap: _onitemTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: _selectedIndex == 0 ? Colors.white : Colors.grey[500],),
            title: Text('Explore', style: TextStyle(color: _selectedIndex == 0 ? Colors.white : Colors.grey[500])),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book, color: _selectedIndex == 1 ? Colors.white : Colors.grey[500]),
            title: Text('Items & Purchases', style: TextStyle(color: _selectedIndex == 1 ? Colors.white : Colors.grey[500])),
          ),
        ],
      ),
    );
  }
}

class page1 extends StatefulWidget {
  @override
  _page1State createState() => _page1State();
}

class _page1State extends State<page1> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
        title: Text('Page 1'),
    backgroundColor: Colors.blueGrey[500],
    ),

    );
  }
}

class page2 extends StatefulWidget {
  @override
  _page2State createState() => _page2State();
}

class _page2State extends State<page2> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Page 2'),
        backgroundColor: Colors.blueGrey[500],
      ),

    );
  }
}
